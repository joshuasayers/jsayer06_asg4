package hotel;



import hotel.checkout.CheckoutCTL;
import hotel.checkout.CheckoutUI;
import hotel.credit.CreditCardType;
import hotel.entities.*;
import hotel.service.RecordServiceCTL;
import hotel.service.RecordServiceUI;
import org.junit.jupiter.api.*;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import java.text.SimpleDateFormat;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;



public class TestServiceChargeBugs {

    @Mock CheckoutUI checkoutUI;
    @Mock RecordServiceUI recordServiceUI;
    Hotel hotel;
    static SimpleDateFormat format;
    CheckoutCTL checkOutControl;
    RecordServiceCTL recordServiceCTL;
    ServiceType serviceType;


    @BeforeAll
    static void setUpBeforeClass() {
        format = new SimpleDateFormat("dd-MM-yyyy");


    }


    @AfterAll
    static void tearDownAfterClass() {
    }


    @BeforeEach
    void setUp() throws Exception{
        MockitoAnnotations.initMocks(this);

        //Hotel Helper creating the pre determined user and service charges, thank you Jim :)
        hotel = HotelHelper.loadHotel();
        checkOutControl = new CheckoutCTL(hotel);
        checkOutControl.checkoutUI = checkoutUI;
        hotel.checkin(11111234101L);





    }


    @AfterEach
    void tearDown() {

    }

    @Test
    void testBug1_NoAmountAddedForServiceCharges() {

        //arrange
        int roomId = 101;
        long confNum = 11111234101L;
        double total = 0;

        Booking booking = hotel.findBookingByConfirmationNumber(confNum);
        hotel.findActiveBookingByRoomId(roomId);
        checkOutControl.state = CheckoutCTL.State.ROOM;

        StringBuilder sb = new StringBuilder();
        sb.append(String.format("Charges for room: %d, booking: %d\n",
                roomId, confNum));

        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        String dateStr = format.format(booking.getArrivalDate());
        sb.append(String.format("Arrival date: %s, Staylength: %d\n", dateStr, booking.getStayLength()));

        Guest guest = booking.getGuest();
        sb.append(String.format("Guest: %s, Address: %s, Phone: %d\n",
                guest.getName(), guest.getAddress(), guest.getPhoneNumber()));

        sb.append("Charges:\n");

        List<ServiceCharge> charges = booking.getCharges();
        for (ServiceCharge sc : charges) {
            total += sc.getCost();
            String chargeStr = String.format("    %-12s:%10s",
                    sc.getDescription(), String.format("$%.2f", sc.getCost()));
            sb.append(chargeStr).append("\n");
        }
        sb.append(String.format("Total: $%.2f\n", total));
        String mesg = sb.toString();



        //act
        assertTrue(checkOutControl.state == CheckoutCTL.State.ROOM);
        checkOutControl.roomIdEntered(booking.getRoomId());
        verify(checkoutUI, times (1)).setState(any());

        //assert
        verify(checkoutUI).displayMessage(mesg);
        verify(checkoutUI).setState(CheckoutUI.State.ACCEPT);
        assertTrue(checkOutControl.state == CheckoutCTL.State.ACCEPT);
      //  assertTrue(total == 18);
        assertTrue(total == 0);

    }

    @Test
    void testBug1_Resolved() {

        //arrange
        int roomId = 101;
        long confNum = 11111234101L;
        double total = 0;

        Booking booking = hotel.findBookingByConfirmationNumber(confNum);
        hotel.findActiveBookingByRoomId(roomId);
        checkOutControl.state = CheckoutCTL.State.ROOM;

        StringBuilder sb = new StringBuilder();
        sb.append(String.format("Charges for room: %d, booking: %d\n",
                roomId, confNum));

        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        String dateStr = format.format(booking.getArrivalDate());
        sb.append(String.format("Arrival date: %s, Staylength: %d\n", dateStr, booking.getStayLength()));

        Guest guest = booking.getGuest();
        sb.append(String.format("Guest: %s, Address: %s, Phone: %d\n",
                guest.getName(), guest.getAddress(), guest.getPhoneNumber()));

        sb.append("Charges:\n");

        List<ServiceCharge> charges = booking.getCharges();
        for (ServiceCharge sc : charges) {
            total += sc.getCost();
            String chargeStr = String.format("    %-12s:%10s",
                    sc.getDescription(), String.format("$%.2f", sc.getCost()));
            sb.append(chargeStr).append("\n");
        }
        sb.append(String.format("Total: $%.2f\n", total));
        String mesg = sb.toString();



        //act
        assertTrue(checkOutControl.state == CheckoutCTL.State.ROOM);
        checkOutControl.roomIdEntered(booking.getRoomId());
        verify(checkoutUI, times (1)).setState(any());

        //assert
        verify(checkoutUI).displayMessage(mesg);
        verify(checkoutUI).setState(CheckoutUI.State.ACCEPT);
        assertTrue(checkOutControl.state == CheckoutCTL.State.ACCEPT);
        assertTrue(total == 18);
        assertFalse(total == 0);

    }


    @Test
    void testBug2_AddServiceChargeToCheckedOutRoom() {

        //arrange
        int roomNumber = 101;
        long confNum = 11111234101L;
        int cardNum = 1;
        int ccv = 1;
        double cost = 25;

        serviceType = ServiceType.ROOM_SERVICE;
        Booking booking = hotel.findBookingByConfirmationNumber(confNum);
        recordServiceCTL = new RecordServiceCTL(hotel);
        recordServiceCTL.recordServiceUI = recordServiceUI;


        checkOutControl.state = CheckoutCTL.State.ROOM;

        assertTrue(checkOutControl.state == CheckoutCTL.State.ROOM);
        checkOutControl.roomIdEntered(booking.getRoomId());
        verify(checkoutUI, times (1)).setState(any());

        assertTrue(checkOutControl.state == CheckoutCTL.State.ACCEPT);
        checkOutControl.chargesAccepted(true);
        verify(checkoutUI, times(2)).setState(any());

        assertTrue(checkOutControl.state == CheckoutCTL.State.CREDIT);
        checkOutControl.creditDetailsEntered(CreditCardType.VISA, cardNum, ccv);
        verify(checkoutUI, times(3)).setState(any());

       // String mesg = String.format("No active booking for room id: %d", roomNumber);
        //act

        assertTrue(checkOutControl.state == CheckoutCTL.State.COMPLETED);

        recordServiceCTL.state = RecordServiceCTL.State.ROOM;

        assertTrue(recordServiceCTL.state == RecordServiceCTL.State.ROOM);
        recordServiceCTL.roomNumberEntered(roomNumber);

       // verify(recordServiceUI).displayMessage(mesg);
        assertTrue(recordServiceCTL.state == RecordServiceCTL.State.SERVICE);

        recordServiceCTL.serviceDetailsEntered(serviceType, cost);


        //assert

        verify(recordServiceUI).displayServiceChargeMessage(roomNumber, cost, serviceType.getDescription());

        assertEquals(101, roomNumber);
        assertEquals(25, cost);
        assertTrue(serviceType == ServiceType.ROOM_SERVICE);
        assertTrue(recordServiceCTL.state == RecordServiceCTL.State.COMPLETED);
        verify(recordServiceUI).setState(RecordServiceUI.State.COMPLETED);


    }

    @Test
    void testBug2_Resolved() {

        //arrange
        int roomNumber = 101;
        long confNum = 11111234101L;
        int cardNum = 1;
        int ccv = 1;
        double cost = 25;

        serviceType = ServiceType.ROOM_SERVICE;
        Booking booking = hotel.findBookingByConfirmationNumber(confNum);
        recordServiceCTL = new RecordServiceCTL(hotel);
        recordServiceCTL.recordServiceUI = recordServiceUI;


        checkOutControl.state = CheckoutCTL.State.ROOM;

        assertTrue(checkOutControl.state == CheckoutCTL.State.ROOM);
        checkOutControl.roomIdEntered(booking.getRoomId());
        verify(checkoutUI, times (1)).setState(any());

        assertTrue(checkOutControl.state == CheckoutCTL.State.ACCEPT);
        checkOutControl.chargesAccepted(true);
        verify(checkoutUI, times(2)).setState(any());

        assertTrue(checkOutControl.state == CheckoutCTL.State.CREDIT);
        checkOutControl.creditDetailsEntered(CreditCardType.VISA, cardNum, ccv);
        verify(checkoutUI, times(3)).setState(any());

        String mesg = String.format("No active booking for room id: %d", roomNumber);
        //act

        assertTrue(checkOutControl.state == CheckoutCTL.State.COMPLETED);

        recordServiceCTL.state = RecordServiceCTL.State.ROOM;

        assertTrue(recordServiceCTL.state == RecordServiceCTL.State.ROOM);
        recordServiceCTL.roomNumberEntered(roomNumber);




        //assert

        verify(recordServiceUI).displayMessage(mesg);
        assertTrue(recordServiceCTL.state == RecordServiceCTL.State.ROOM);

    }



}

