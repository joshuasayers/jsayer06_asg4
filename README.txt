Hi Jim and/or other students of Jim!

Firstly I would like to say thank you for all your help this semester.
You are a fantastic lecturer and I enjoyed your classes immensely.

Moving right along to assignment 4 details, in this repository you will find the following folders in the outlined structure and includes: The UAT bug tests, Automation Tests bugs, Debugging logs, Debugging screenshots (also in the logs but in case you couldn�t view them), Automation Fail Tests for Resolution, UAT bug fix tests and UAT bug fix screenshots.



-DebugLogs (3. Tracing - contains 2 bugging logs) 
-DebugScreenShots (3. Tracing - contains 18 screenshots also included in debugging logs)
-ResolutionOutputFiles (4. Resolution - contains 2 Automation test screenshots for bug resolution, 2 screenshots for UAT testing, 1 UAT test case for bug 1/2 resolution )
-Tests (2. Simplification - contains 1 test file for Automation bug testing AND bug resolution)
-UAT Replication (1. Replication - contains 1 UAT test case for bug 1/2)


